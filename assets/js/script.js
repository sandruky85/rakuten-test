 $(function() {

    var owner = $('#owner');
    var cardNumber = $('#cardNumber');
    var cardNumberField = $('#card-number-field');
    var CVV = $("#cvv");
    var mastercard = $("#mastercard");
    var confirmButton = $('#confirm-purchase');
    var visa = $("#visa");
    var amex = $("#amex");

    var shipping = [];

    shipping['DE'] = [2.5, '€'];
    shipping['AT'] = [3, '€'];
    shipping['ES'] = [5.45, '€'];
    shipping['FR'] = [2, '€'];
    shipping['UK'] = [2.75, 'GBP'];

    $("#city").prop('disabled', true);
    confirmButton.prop('disabled', true);

    $("#term:checkbox").change(function(){
        if($(this).is(':checked')){
            confirmButton.prop('disabled', false);
        } else {
            confirmButton.prop('disabled', true);
        }
            
    });

    $("#country").on("change", function(){
        $("#city").prop('disabled', false);
    });

    $("#city").on("change", function(){
        var sc = shipping[$("#country").val()]
        $("#scost").val(sc[0]+ " "+sc[1]);
    });

    // Use the payform library to format and validate
    // the payment fields.

    cardNumber.payform('formatCardNumber');
    CVV.payform('formatCardCVC');


    cardNumber.keyup(function() {

        amex.removeClass('transparent');
        visa.removeClass('transparent');
        mastercard.removeClass('transparent');

        if ($.payform.validateCardNumber(cardNumber.val()) == false) {
            cardNumberField.addClass('has-error');
        } else {
            cardNumberField.removeClass('has-error');
            cardNumberField.addClass('has-success');
        }

        if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
            mastercard.addClass('transparent');
            amex.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
            mastercard.addClass('transparent');
            visa.addClass('transparent');
        } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
            amex.addClass('transparent');
            visa.addClass('transparent');
        }
    });

    $("#paymentform").validate({
        submitHandler: function(form) {
            $("#paymentform").on("click",function(e) {

            e.preventDefault();
    
            var isCardValid = $.payform.validateCardNumber(cardNumber.val());
            var isCvvValid = $.payform.validateCardCVC(CVV.val());

            if (!isCardValid) {
                $("#card-error").show();
            } else if (!isCvvValid) {
                $("#card-error").show();
            } else {
                $.post( "thank-you.html", function( data ) {
                    $( ".container-fluid" ).html( data );
                  });
            }
        });
        }
       });
       
    $("#city").autocomplete({
		source: function (request, response) {
		 jQuery.getJSON(
            "http://gd.geobytes.com/AutoCompleteCity?callback=?&filter="+$("#country").val()+"&template=<geobytes%20city>,%20<geobytes%20code>&q="+request.term,
			function (data) {
			 response(data);
			}
		 );
		},
		minLength: 3,
		select: function (event, ui) {
		 var selectedObj = ui.item;
		 $("#city").val(selectedObj.value);
		 return false;
		},
		open: function () {
		 $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close: function () {
		 $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	 });
     $("#city").autocomplete("option", "delay", 100);

});



